import contract from 'truffle-contract';

import SimpleStorageContract from 'build/contracts/SimpleStorage.json'

const initialState = {
  contract: null,
  value: null,
};

const ACTIONS = {
  SET_CONTRACT: 'CONTRACT/SET_CONTRACT',
  GET_VALUE: 'CONTRACT/GET_VALUE',
  SET_VALUE: 'CONTRACT/SET_VALUE'
};

export const setContract = contract => ({
  type: ACTIONS.SET_CONTRACT,
  contract
});

export const setValue = value => ({
  type: ACTIONS.SET_VALUE,
  value
});

export const getValue = () => {
  return (dispatch, getState) => {
    const state = getState();
    const web3 = state.web3.web3;
    const contract = state.contract.contract;
  
    web3.eth.getAccounts((error, accounts) => {
      contract.get.call(accounts[0])
        .then(result => {
          const value = result.c[0];
          dispatch(setValue(value));
        });
    });
  }
};

export const saveValue = value => {
  return (dispatch, getState) => {
    const state = getState();
    const web3 = state.web3.web3;
    const contract = state.contract.contract;

    web3.eth.getAccounts((error, accounts) => {
      contract.set(value, { from: accounts[0] })
    });
  }
};

export const instantiateContract = () => {
  return (dispatch, getState) => {
    const state = getState();
    const web3 = state.web3.web3;
    const simpleStorage = contract(SimpleStorageContract)
    simpleStorage.setProvider(web3.currentProvider)

    return simpleStorage.deployed()
      .then(instance => {
        dispatch(setContract(instance));
      });
  }
}

const smartContractReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_CONTRACT:
      return Object.assign({}, state, {
        contract: action.contract,
      });
    case ACTIONS.SET_VALUE:
      return Object.assign({}, state, {
        value: action.value
      });
    default:
      return state;
  }
};

export default smartContractReducer;
