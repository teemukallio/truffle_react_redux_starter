import getWeb3 from 'utils/getWeb3'

const initialState = {
  web3: null,
};

const ACTIONS = {
  SET_WEB3: 'WEB3/SET_WEB3',
};

export const setWeb3 = web3 => ({
  type: ACTIONS.SET_WEB3,
  web3,
});

export const initWeb3 = () => {
  return dispatch => {
    return getWeb3
    .then(results => {
      const { web3 } = results;
      dispatch(setWeb3(web3));
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_WEB3:
      return Object.assign({}, state, {
        web3: action.web3,
      });
    default:
      return state;
  }
};

