import { combineReducers } from 'redux'

import contract from './modules/contract.js';
import web3 from './modules/web3.js';

export default combineReducers({
  contract,
  web3
});
