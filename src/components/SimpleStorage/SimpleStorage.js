import React from 'react'

export default ({ value, saveValue }) => {
  let input;

  const save = () => {
    saveValue(input.value);
  };

  return (
    <div className="simple-storage">
      <p>Value: { value }</p>
      <input type="text" ref={node => {input = node}}></input>
      <button onClick={save}>Set</button>
    </div>
  );
}
