import { connect } from 'react-redux';

import { saveValue } from 'reducers/modules/contract.js';
import SimpleStorage from './SimpleStorage.js';

const mapStateToProps = state => ({
  contract: state.contract.contract,
  value: state.contract.value,
});

const mapDispatchToProps = dispatch => ({
  saveValue: value => dispatch(saveValue(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SimpleStorage);

