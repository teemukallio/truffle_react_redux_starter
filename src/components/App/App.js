import React, { Component } from 'react'

import './App.css'

import SimpleStorage from 'components/SimpleStorage';

class App extends Component {
  componentWillMount() {
    const {
      initWeb3,
      instantiateContract,
      getStorageValue
    } = this.props;

    initWeb3()
      .then(() => {
        instantiateContract()
          .then(() => {
            getStorageValue()
          });
      });
  }

  render() {
    const {
      contract
    } = this.props;

    const component = contract
      ? <SimpleStorage />
      : <h1>Waiting for Web3</h1>

    return (
      <div className="App">
        <main className="container">
          { component }
        </main>
      </div>
    );
  }
}

export default App
