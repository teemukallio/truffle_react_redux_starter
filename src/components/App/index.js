import { connect } from 'react-redux';

import { initWeb3 } from 'reducers/modules/web3.js';
import {
  instantiateContract,
  getValue
} from 'reducers/modules/contract.js';
import App from './App.js';

const mapStateToProps = state => ({
  web3: state.web3.web3,
  contract: state.contract.contract,
});

const mapDispatchToProps = dispatch => ({
  initWeb3: web3 => dispatch(initWeb3()),
  instantiateContract: () => dispatch(instantiateContract()),
  getStorageValue: () => dispatch(getValue())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
